#!/bin/bash

memory=$(awk '{OFMT = "%.0f"}NR==1{t = $2}NR==3{print (t-$2)/1024}' /proc/meminfo)
memoryPercent=$(awk '{OFMT = "%.0f"}NR==1{t = $2}NR==3{print 100*(t-$2)/t}' /proc/meminfo)
if   [ $memoryPercent -gt 87 ]; then
	memoryIcon=""
elif [ $memoryPercent -gt 75 ]; then
	memoryIcon=""
elif [ $memoryPercent -gt 62 ]; then
	memoryIcon=""
elif [ $memoryPercent -gt 50 ]; then
	memoryIcon=""
elif [ $memoryPercent -gt 37 ]; then
	memoryIcon=""
elif [ $memoryPercent -gt 25 ]; then
	memoryIcon=""
elif [ $memoryPercent -gt 12 ]; then
	memoryIcon=""
else
	memoryIcon=""
fi

storage=$(df | awk '{OFMT = "%.1f"}$NF=="/"{print $3/1048576}')
storagePercent=$(df | awk '{OFMT = "%.0f"}$NF=="/"{print 100*$3/($3+$4)}')
if   [ $storagePercent -gt 87 ]; then
	storageIcon=""
elif [ $storagePercent -gt 75 ]; then
	storageIcon=""
elif [ $storagePercent -gt 62 ]; then
	storageIcon=""
elif [ $storagePercent -gt 50 ]; then
	storageIcon=""
elif [ $storagePercent -gt 37 ]; then
	storageIcon=""
elif [ $storagePercent -gt 25 ]; then
	storageIcon=""
elif [ $storagePercent -gt 12 ]; then
	storageIcon=""
else
	storageIcon=""
fi

temperature=$(($(</sys/class/thermal/thermal_zone4/temp)/1000))
if   [ $temperature -gt 87 ]; then
	temperatureIcon=""
elif [ $temperature -gt 75 ]; then
	temperatureIcon=""
elif [ $temperature -gt 62 ]; then
	temperatureIcon=""
elif [ $temperature -gt 50 ]; then
	temperatureIcon=""
elif [ $temperature -gt 37 ]; then
	temperatureIcon=""
elif [ $temperature -gt 25 ]; then
	temperatureIcon=""
elif [ $temperature -gt 12 ]; then
	temperatureIcon=""
else
	temperatureIcon=""
fi

wireless=$(awk 'NR==3 {printf "%d", ($3/70)*100}' /proc/net/wireless)
if [ "$wireless" != "" ]; then
	if   [ $wireless -gt 87 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 75 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 62 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 50 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 37 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 25 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 12 ]; then
		wirelessIcon=""
	else
		wirelessIcon=""
	fi
	wireless+="%%  "
fi

battery=$(</sys/class/power_supply/BAT0/capacity)
if   [ $battery -gt 87 ]; then
	batteryIcon=""
elif [ $battery -gt 75 ]; then
	batteryIcon=""
elif [ $battery -gt 62 ]; then
	batteryIcon=""
elif [ $battery -gt 50 ]; then
	batteryIcon=""
elif [ $battery -gt 37 ]; then
	batteryIcon=""
elif [ $battery -gt 25 ]; then
	batteryIcon=""
elif [ $battery -gt 12 ]; then
	batteryIcon=""
else
	batteryIcon=""
fi

printf " ${memoryIcon}${memory}MB  ${storageIcon}${storage}GB  ${temperatureIcon}${temperature}C  ${wirelessIcon}${wireless}${batteryIcon}${battery}%%  $(date '+%a %d %b  %H:%M:%S')"
