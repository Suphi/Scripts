#!/bin/bash

drive="mmcblk0"
timezone="Asia/Istanbul"
localization="en_GB.UTF-8"
keyboardlayout="uk"
xkeyboardlayout="gb"
hostname="E402NA"
swapsize="512"
bootoptions="mitigations=off nmi_watchdog=0 i915.enable_guc=2 i915.enable_fbc=1 i915.fastboot=1 quiet"
configdirectory="Configurations"
install="
	base base-devel linux linux-lts linux-firmware linux-firmware-qlogic intel-ucode efibootmgr
	xf86-video-intel libva-intel-driver xf86-input-synaptics pulseaudio networkmanager dnsmasq
	xorg-server xorg-xinit xorg-xinput xorg-xbacklight libxft libxinerama
	fish nano git xwallpaper xdg-user-dirs zip unzip gst-libav
	thunar thunar-volman gvfs gvfs-mtp gvfs-gphoto2 ntfs-3g thunar-archive-plugin tumbler ffmpegthumbnailer xarchiver
	dmenu nm-connection-editor blueman pavucontrol lxtask lxinput lxrandr lxappearance gcolor2 xfce4-screenshooter
	alacritty chromium mousepad eom gimp inkscape pragha parole galculator transmission-gtk blender
	ttf-dejavu gnome-themes-extra papirus-icon-theme
"
aurinstall="
	https://aur.archlinux.org/aic94xx-firmware.git
	https://aur.archlinux.org/wd719x-firmware.git
	https://aur.archlinux.org/upd72020x-fw.git
	https://aur.archlinux.org/ast-firmware.git
	https://aur.archlinux.org/wireless-regdb-pentest.git
	https://aur.archlinux.org/betaflight-configurator-bin.git
"
gitinstall="
	https://gitlab.com/Suphi/DynamicWindowManager.git
	https://gitlab.com/Suphi/DynamicStatus.git
	https://gitlab.com/Suphi/StatusIconFonts.git
	https://gitlab.com/Suphi/Tello.git
"
gitdownload="
	https://gitlab.com/Suphi/Scripts.git
	https://gitlab.com/Suphi/Configurations.git
"
service="
	NetworkManager.service
	dnsmasq.service
	bluetooth.service
"
if [ "$1" == "" ]; then
	loadkeys ${keyboardlayout}

	echo "Enter user name"
	read user
	echo "Enter ${user}'s password"
	read userpassword
	echo "Enter root password"
	read rootpassword

	timedatectl set-ntp true
	timedatectl set-timezone ${timezone}

	parted -s /dev/${drive} mklabel gpt mkpart ESP fat32 0% 256MiB mkpart primary ext4 256MiB 100% set 1 boot on
	mkfs.fat -F32 /dev/${drive}p1 > /dev/null
	mkfs.ext4 -F /dev/${drive}p2 > /dev/null 2> /dev/null
	mount /dev/${drive}p2 /mnt
	mkdir -p /mnt/boot
	mount /dev/${drive}p1 /mnt/boot

	pacstrap /mnt ${install}

	genfstab -U /mnt >> /mnt/etc/fstab

	cp $0 /mnt/setup
	arch-chroot /mnt ./setup ${user} ${userpassword} ${rootpassword}
	rm /mnt/setup

	umount -R /mnt
else
	user=${1}
	userpassword=${2}
	rootpassword=${3}

	ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
	hwclock --systohc

	sed -i "/${localization}/s/^#//g" /etc/locale.gen
	echo "LANG=${localization}" > /etc/locale.conf
	locale-gen > /dev/null

	echo "KEYMAP=${keyboardlayout}" > /etc/vconsole.conf

	echo "${hostname}" > /etc/hostname
	echo "127.0.0.1  localhost" > /etc/hosts
	echo "::1        localhost" >> /etc/hosts
	echo "127.0.1.1  ${hostname}.localdomain  ${hostname}" >> /etc/hosts

	echo "EDITOR=nano" >> /etc/environment

	sed -i "/%wheel ALL=(ALL) NOPASSWD: ALL/s/^# //g" /etc/sudoers

	echo "Section  \"Device\"" > /etc/X11/xorg.conf.d/graphics.conf
	echo "  Identifier  \"Intel Graphics\"" >> /etc/X11/xorg.conf.d/graphics.conf
	echo "  Driver      \"intel\"" >> /etc/X11/xorg.conf.d/graphics.conf
	echo "  Option      \"TearFree\"  \"true\"" >> /etc/X11/xorg.conf.d/graphics.conf
	echo "EndSection" >> /etc/X11/xorg.conf.d/graphics.conf

	echo "Section  \"InputClass\"" > /etc/X11/xorg.conf.d/keyboard.conf
	echo "  Identifier       \"system-keyboard\"" >> /etc/X11/xorg.conf.d/keyboard.conf
	echo "  MatchIsKeyboard  \"on\"" >> /etc/X11/xorg.conf.d/keyboard.conf
	echo "  Option           \"XkbLayout\"  \"${xkeyboardlayout}\"" >> /etc/X11/xorg.conf.d/keyboard.conf
	echo "EndSection" >> /etc/X11/xorg.conf.d/keyboard.conf

	echo "Section  \"InputClass\"" > /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Identifier       \"touchpad\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Driver           \"synaptics\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  MatchIsTouchpad  \"on\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Option           \"TapButton3\"            \"2\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Option           \"VertTwoFingerScroll\"   \"on\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Option           \"HorizTwoFingerScroll\"  \"on\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Option           \"VertScrollDelta\"       \"-111\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "  Option           \"HorizScrollDelta\"      \"-111\"" >> /etc/X11/xorg.conf.d/touchpad.conf
	echo "EndSection" >> /etc/X11/xorg.conf.d/touchpad.conf

	echo "options snd_hda_intel power_save=1" > /etc/modprobe.d/audio-powersave.conf
	echo "ACTION==\"add\", SUBSYSTEM==\"pci\", ATTR{power/control}=\"auto\"" > /etc/udev/rules.d/pci-powersave.rules
	echo "ACTION==\"add\", SUBSYSTEM==\"scsi_host\", KERNEL==\"host*\", ATTR{link_power_management_policy}=\"med_power_with_dipm\"" > /etc/udev/rules.d/sata-powersave.rules
	echo "ACTION==\"add\", SUBSYSTEM==\"net\", KERNEL==\"wl*\", RUN+=\"/usr/bin/iw dev $name set power_save on\"" > /etc/udev/rules.d/81-wifi-powersave.rules
	echo "vm.dirty_writeback_centisecs = 3000" > /etc/sysctl.d/dirty.conf
	echo "vm.laptop_mode = 5" > /etc/sysctl.d/laptop.conf

	dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} status=progress
	chmod 600 /swapfile
	mkswap /swapfile > /dev/null
	echo "# /swapfile" >> /etc/fstab
	echo "/swapfile    none    swap    defaults    0 0" >> /etc/fstab

	for ((i=0; i<=10; i++)); do
		efibootmgr -Bb ${i} > /dev/null 2> /dev/null
	done
	partuuid=$(blkid -o value -s PARTUUID /dev/${drive}p2)
	efibootmgr -c -g -d /dev/${drive} -p 1 -L "Linux" -l /vmlinuz-linux -u "root=PARTUUID=${partuuid} rw ${bootoptions} initrd=/intel-ucode.img initrd=/initramfs-linux.img" > /dev/null
	efibootmgr -c -g -d /dev/${drive} -p 1 -L "Linux LTS" -l /vmlinuz-linux-lts -u "root=PARTUUID=${partuuid} rw ${bootoptions} initrd=/intel-ucode.img initrd=/initramfs-linux-lts.img" > /dev/null
	efibootmgr -o 0,1 > /dev/null

	useradd -m -G wheel uucp -s /bin/fish ${user}
	sudo -u ${user} xdg-user-dirs-update
	eval userpath=~${user}

	mkdir -p /etc/systemd/system/getty@tty1.service.d
	echo "[Service]" > /etc/systemd/system/getty@tty1.service.d/override.conf
	echo "ExecStart=" >> /etc/systemd/system/getty@tty1.service.d/override.conf
	echo "ExecStart=-/usr/bin/agetty --autologin ${user} --noclear %I $TERM" >> /etc/systemd/system/getty@tty1.service.d/override.conf

	for item in ${aurinstall}; do
		name=$(basename ${item} .git)
		echo "Installing ${name}"
		cd /tmp
		sudo -u ${user} git clone ${item} 2> /dev/null
		cd ${name}
		sudo -u ${user} makepkg -sirc --noconfirm > /dev/null 2> /dev/null
		cd /tmp
		rm -rf ${name}
	done

	for item in ${gitinstall}; do
		name=$(basename ${item} .git)
		echo "Installing ${name}"
		cd ${userpath}/Documents
		sudo -u ${user} git clone ${item} 2> /dev/null
		cd ${name}
		make install > /dev/null
	done

	for item in ${gitdownload}; do
		name=$(basename ${item} .git)
		echo "Downloading ${name}"
		cd ${userpath}/Documents
		sudo -u ${user} git clone ${item} 2> /dev/null
	done

	if [ "$configdirectory" != "" ]; then
		cp -a ${userpath}/Documents/${configdirectory}/. ${userpath}/
	fi

	for item in ${service}; do
		systemctl enable ${item} 2> /dev/null
	done

	echo -en "${rootpassword}\n${rootpassword}" | passwd > /dev/null 2> /dev/null
	echo -en "${userpassword}\n${userpassword}" | passwd ${user} > /dev/null 2> /dev/null
fi
