#!/bin/bash

getBacklight() {
	xbacklight | cut -d '.' -f1
}

xbacklight -time 0 $1 $2
dstatus n 2 "$(getBacklight)%  "
