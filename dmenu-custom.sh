#!/bin/sh

namelist=(
	"Browser"
	"Calculator"
	"File Editor"
	"File Manager"
	"Image Editor"
	"Music Player"
	"Terminal"
	"Torrents"
	"Vector Editor"
	"Video Player"
	"Model Editor"
	"Audio Manager"
	"Bluetooth Manager"
	"Network Manager"
	"Display Manager"
	"Input Manager"
	"Task Manager"
	"Theme Manager"
)

commandlist=(
	"chromium"
	"galculator"
	"mousepad"
	"thunar"
	"gimp"
	"pragha"
	"alacritty"
	"transmission-gtk"
	"inkscape"
	"parole"
	"blender"
	"pavucontrol"
	"blueman-manager"
	"nm-connection-editor"
	"lxrandr"
	"lxinput"
	"lxtask"
	"lxappearance"
)

sel=$(printf '%s\n' "${namelist[@]}" | dmenu "$@")

if [ "$sel" ]; then
	for i in "${!namelist[@]}"; do
		if [ "${namelist[$i]}" = "$sel" ]; then
			break
		fi
	done

	${commandlist[$i]}
fi
