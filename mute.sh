#!/bin/bash

getDefaultSink() {
	pactl info | awk '/^Default Sink: / {print $3; exit}'
}

getMute() {
	pactl list | awk '/^\s+Name: / {found = $2 == "'$defaultSink'"} /^\s+Mute: / && found {print $2; exit}'
}

setMute() {
	pactl set-sink-mute $defaultSink $1
}

defaultSink=$(getDefaultSink)
setMute $1
if [ "$(getMute)" == "yes" ]; then
	dstatus n 2 "Muted  "
else
	dstatus n 2 "Unmuted  "
fi
