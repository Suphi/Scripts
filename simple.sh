#!/bin/bash

memory=$(awk '{OFMT = "%.0f"}NR==1{t = $2}NR==3{print 100*(t-$2)/t}' /proc/meminfo)
if   [ $memory -gt 87 ]; then
	memoryIcon=""
elif [ $memory -gt 75 ]; then
	memoryIcon=""
elif [ $memory -gt 62 ]; then
	memoryIcon=""
elif [ $memory -gt 50 ]; then
	memoryIcon=""
elif [ $memory -gt 37 ]; then
	memoryIcon=""
elif [ $memory -gt 25 ]; then
	memoryIcon=""
elif [ $memory -gt 12 ]; then
	memoryIcon=""
else
	memoryIcon=""
fi

storage=$(df | awk '{OFMT = "%.0f"}$NF=="/"{print 100*$3/($3+$4)}')
if   [ $storage -gt 87 ]; then
	storageIcon=""
elif [ $storage -gt 75 ]; then
	storageIcon=""
elif [ $storage -gt 62 ]; then
	storageIcon=""
elif [ $storage -gt 50 ]; then
	storageIcon=""
elif [ $storage -gt 37 ]; then
	storageIcon=""
elif [ $storage -gt 25 ]; then
	storageIcon=""
elif [ $storage -gt 12 ]; then
	storageIcon=""
else
	storageIcon=""
fi

temperature=$(($(</sys/class/thermal/thermal_zone4/temp)/1000))
if   [ $temperature -gt 87 ]; then
	temperatureIcon=""
elif [ $temperature -gt 75 ]; then
	temperatureIcon=""
elif [ $temperature -gt 62 ]; then
	temperatureIcon=""
elif [ $temperature -gt 50 ]; then
	temperatureIcon=""
elif [ $temperature -gt 37 ]; then
	temperatureIcon=""
elif [ $temperature -gt 25 ]; then
	temperatureIcon=""
elif [ $temperature -gt 12 ]; then
	temperatureIcon=""
else
	temperatureIcon=""
fi

wireless=$(awk 'NR==3 {printf "%d", ($3/70)*100}' /proc/net/wireless)
if [ "$wireless" != "" ]; then
	if   [ $wireless -gt 87 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 75 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 62 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 50 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 37 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 25 ]; then
		wirelessIcon=""
	elif [ $wireless -gt 12 ]; then
		wirelessIcon=""
	else
		wirelessIcon=""
	fi
fi

sink=$(pactl info | awk '/^Default Sink: / {print $3; exit}')
volume=$(pactl list | awk '/^\s+Name: / {found = $2 == "'$sink'"} /^\s+Volume: / && found {print substr($5, 1, length($5)-1); exit}')
if   [ $volume -gt 87 ]; then
	volumeIcon=""
elif [ $volume -gt 75 ]; then
	volumeIcon=""
elif [ $volume -gt 62 ]; then
	volumeIcon=""
elif [ $volume -gt 50 ]; then
	volumeIcon=""
elif [ $volume -gt 37 ]; then
	volumeIcon=""
elif [ $volume -gt 25 ]; then
	volumeIcon=""
elif [ $volume -gt 12 ]; then
	volumeIcon=""
else
	volumeIcon=""
fi

battery=$(</sys/class/power_supply/BAT0/capacity)
if   [ $battery -gt 87 ]; then
	batteryIcon=""
elif [ $battery -gt 75 ]; then
	batteryIcon=""
elif [ $battery -gt 62 ]; then
	batteryIcon=""
elif [ $battery -gt 50 ]; then
	batteryIcon=""
elif [ $battery -gt 37 ]; then
	batteryIcon=""
elif [ $battery -gt 25 ]; then
	batteryIcon=""
elif [ $battery -gt 12 ]; then
	batteryIcon=""
else
	batteryIcon=""
fi

printf " ${memoryIcon}${storageIcon}${temperatureIcon}${wirelessIcon}${volumeIcon}${batteryIcon}$(date '+%H:%M')"