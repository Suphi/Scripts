#!/bin/bash

if lsmod | grep "i2c_hid" &> /dev/null; then
	sudo modprobe -r i2c_hid
	dstatus n 2 "Touchpad Disabled  "
else
	sudo modprobe i2c_hid
	dstatus n 2 "Touchpad Enabled  "
fi