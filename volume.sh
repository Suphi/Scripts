#!/bin/bash

getDefaultSink() {
	pactl info | awk '/^Default Sink: / {print $3; exit}'
}

getVolume() {
	pactl list | awk '/^\s+Name: / {found = $2 == "'$defaultSink'"} /^\s+Volume: / && found {print $5; exit}'
}

setVolume() {
	pactl set-sink-volume $defaultSink $1
}

defaultSink=$(getDefaultSink)
setVolume $1
dstatus u 2 "$(getVolume)  "
