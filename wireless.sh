#!/bin/bash

. /usr/lib/netctl/globals
. "$SUBR_DIR/interface"
. "$SUBR_DIR/wpa"

INTERFACE=$1
if [[ -z "$INTERFACE" ]]; then
	INTERFACE=(/sys/class/net/*/wireless/)
	if [[ "${#INTERFACE[@]}" -ne 1 || ! -d "$INTERFACE" ]]; then
		echo "Invalid interface specification"
		exit
	fi
	INTERFACE=${INTERFACE:15:-10}
elif ! is_interface "$INTERFACE"; then
	echo "No such interface: $INTERFACE"
	exit
fi

dstatus n 5 "Scanning  "

NETWORKS=$(wpa_supplicant_scan "$INTERFACE" 3,4,5)
RETURN=$?

if (( RETURN == 0 )); then
	trap 'rm -f "$NETWORKS"' EXIT

	i=0
	while read -r profile; do
		source "$PROFILE_DIR/$profile" > /dev/null
		if [[ "$Interface" = "$INTERFACE" && -n "$ESSID" ]]; then
			ESSIDS[i]="$ESSID"
			PROFILES[i++]="$profile"
		fi
	done < <(list_profiles)

	i=0
	while IFS=$'\t' read -r signal flags ssid; do
		if in_array "$ssid" "${ESSIDS[@]}"; then
			FESSIDS[i]="$ssid"
			FSIGNAL[i++]="$signal"
		fi
	done < "$NETWORKS"

	i=0
	BSIGNAL="-100"
	for c in "${FESSIDS[@]}"; do
		if [[ "${FSIGNAL[i]}" -gt "$BSIGNAL" ]]; then
			BSIGNAL="${FSIGNAL[i]}"
			BESSID="${FESSIDS[i]}"
		fi
		(( i++ ))
	done

	if [[ -n "$BESSID" ]]; then
		CONNECTION=$(wpa_call "$INTERFACE" status 2> /dev/null | sed -n "s/^ssid=//p")
		if [[ "$BESSID" = "$CONNECTION" ]]; then
			dstatus n 2  "Already connected ($BESSID)  "
		else
			dstatus u 5 "Connecting ($BESSID)  "
			i=0
			for c in "${ESSIDS[@]}"; do
				if [[ "${ESSIDS[i]}" = "$BESSID" ]]; then
					BPROFILE="${PROFILES[i]}"
					break;
				fi
				(( i++ ))
			done
			netctl switch-to "$BPROFILE"
			dstatus n 2 "Connected ($BESSID)  "
		fi
	fi
fi
